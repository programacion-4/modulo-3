from pymongo import MongoClient
class MONGO():
    def __init__(self,nombreDB,host,port):
        self.mongoUser = MongoClient(host,int(port))
        self.db = self.mongoUser[nombreDB]
        self.database = self.db.myTableName

    def post(self,palabra,definicion):
        try:
            data = {'palabra':palabra,'definicion':definicion}
            self.database.insert_one(data)
            print('La palabra se añadido exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)
    
    def getDefinicion(self,palabra):
        try:
            data = {'palabra':palabra}
            resultado = self.database.find_one(data)
            print('Definicion:'+resultado['definicion'])
        except Exception as err:
            print('algo salio mal:')
            print(err)
    
    def getAllPalabras(self):
        try:
            resultado = self.database.find()
            print('Lista de palabras')
            for infmongoacion in resultado:
                print(infmongoacion['palabra']+'\n')
        except Exception as err:
            print('algo salio mal:')
            print(err)
    
    def updatePalabra(self,palabra,newPalabra):
        try:
            data = {'palabra':palabra}
            newData = {'palabra':newPalabra}
            self.database.update_one(data,{"$set":newData})
            print('La palabra se actualizado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)

    def updateDefinicion(self,palabra,newDefinicion):
        try:
            data = {'palabra':palabra}
            newData = {'definicion':newDefinicion}
            self.database.update_one(data,{"$set":newData})
            print('La definicion se actualizado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)

    def delete(self,palabra):
        try:
            data = {'palabra':palabra}
            self.database.delete_many(data)
            print('La palabra se eliminado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)

def main(nombreDB,host,port):
    try:
        mongo = MONGO(nombreDB,host,port)
        exit = False
        op = input('1-Agregar palabra  \n2-Actualizar palabra  3-Actualizar definicion  \n4-Lista de palabras  5-Buscar difinicion  \n6-Eliminar palabra   7-SALIR 🏃‍♂️ \n')
        if(str(op) == '1'):
            print('⚠  RES:')
            mongo.post(input('introduzca la palabra:'),input('introduzca su definicion:'))
            
        elif(str(op) == '2'):
            print('⚠  RES:')
            mongo.updatePalabra(input('introduzca la palabra que quiera actualizar:'),input('introduzca la nueva palabra:'))
            
        elif(str(op) == '3'):
            print('⚠  RES:')
            mongo.updateDefinicion(input('introduzca la palabra que quiera actualizar su definicion:'),input('introduzca la nueva definicion:'))
            
        elif(str(op) == '4'):
            print('⚠  RES:')
            mongo.getAllPalabras()
            
        elif(str(op) == '5'):
            print('⚠  RES:')
            mongo.getDefinicion(input('introduzca la palabra que quiera buscar la definicion:'))
            
        elif(str(op) == '6'):
            print('⚠  RES:hay veces en la que debe salir para que se apliquen los cambios')
            mongo.delete(input('introduzca la palabra que quieras borrar:'))
        
        elif(str(op) == '7'):
            exit = True
        if(not exit):
            main(nombreDB,host,port)
        print('⚠  RES:Saliendo')
    except Exception as err:
            print('algo salio mal:')
            print(err)
    


if __name__ == "__main__":
    print('\nBIENCENIDO AL DICCIONARIO DE SLANG PANAMEÑO ✨\n')
    print('nota:"host por defecto:localhost el puerto por defecto:27017"')
    main(input('Itroduzca nombre de la base de datos:'),input('Itroduzca el host:'),input('Itroduzca el puerto:'),)
